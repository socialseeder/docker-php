FROM php:7.4-fpm

RUN apt-get update && \
    apt-get install -y --no-install-recommends git libzip-dev zip unzip libpng-dev libjpeg-dev

RUN docker-php-ext-configure gd --with-jpeg=/usr/include/

RUN docker-php-ext-install mysqli pdo_mysql gd pcntl zip

# Locked to composer 1 for now, as we still have dependencies that are not composer 2 compatible.
# @TODO Remove -- --1 later on.
RUN curl -sS https://getcomposer.org/installer | php -- --1 && \
    mv composer.phar /usr/local/bin/ && \
    ln -s /usr/local/bin/composer.phar /usr/local/bin/composer
